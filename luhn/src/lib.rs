fn calculate_luhn(code: String) -> bool {
   let check:u32 = code.chars()
                    .rev()
                    .map(|x| x.to_digit(10).unwrap())
                    .enumerate()
                    .map(|(index, num)| 
                            match (index % 2, Some(num)) {
                                (0, Some(x)) => x,
                                (1, Some(x)) if x > 4 => (x * 2) - 9,
                                (1, Some(x)) => x * 2,
                                (_, _) => 0
                    })
                    .sum();

    check % 10 == 0                
}

/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let has_only_digits = |x: &str| x.chars().map(|x| x.is_numeric()).fold(true, |acc, x| x && acc);
    let code = code.replace(" ", "");

    code.trim().len() > 1 
        && has_only_digits(&code)
        && calculate_luhn(code)
}