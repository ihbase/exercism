use luhn::is_valid;

fn main() {
  println!("Single value: {}", is_valid("1"));
  println!("45X9 3195 0343 6467: {}", is_valid("45X9 3195 0343 6467"));
  println!("4539 3195 0343 6467: {}", is_valid("4539 3195 0343 6467"));
}