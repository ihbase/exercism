pub fn raindrops(n: u32) -> String {
    let rd = [(3, "Pling"), (5, "Plang"), (7,"Plong")]
        .iter()
        .filter_map(|&(factor, sound)| if n % factor == 0 { Some(sound) } else { None } )
        .collect::<String>();

    match rd.is_empty() {
       true => n.to_string(),
       false => rd.to_string()
    }
}
