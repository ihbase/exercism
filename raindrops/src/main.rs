use raindrops::raindrops;

fn main() {
    println!("{}", raindrops(3));
    println!("{}", raindrops(5));
    println!("{}", raindrops(7));
    println!("{}", raindrops(15));
    println!("{}", raindrops(21));
    println!("{}", raindrops(35));
    println!("{}", raindrops(105));

    println!("34: {}", raindrops(34));
}