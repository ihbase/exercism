fn abbreviate_word(word: &str) -> Option<String> {
    let res:String = word.clone().trim().chars()
        .filter(|x| x.is_uppercase())
        .collect();

    match (res.len() > 0, Some(word)) {
        (true, Some(x)) if res == x => Some(x.chars().nth(0).unwrap().to_string()),
        (true, _) => Some(res),
        (_ , _) => Some(word.chars().nth(0).unwrap().to_string())
    }
}

pub fn abbreviate(phrase: &str) -> String {
    let normalize = |p: &str| p.chars().map(|x| if x.is_ascii_punctuation() { ' ' } else { x }).collect::<String>();

    normalize(&phrase.replace('\'', ""))
        .split(" ")
        .filter(|x| x.trim().len() > 0 )
        .flat_map(|x| abbreviate_word(x))
        .collect::<String>()
        .to_uppercase()
}
