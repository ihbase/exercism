fn main() {
  assert_eq!(acronym::abbreviate(""), "");
  assert_eq!(acronym::abbreviate("Portable Network Graphics"), "PNG");
  assert_eq!(acronym::abbreviate("PHP: Hypertext Preprocessor"), "PHP");
  assert_eq!(acronym::abbreviate("Ruby on Rails"), "ROR");
}